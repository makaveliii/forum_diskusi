-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2020 at 09:51 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opbright`
--

-- --------------------------------------------------------

--
-- Table structure for table `forum_index`
--

CREATE TABLE `forum_index` (
  `id_forum` int(11) NOT NULL,
  `id_topics` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` varchar(30) NOT NULL,
  `action` int(3) DEFAULT NULL,
  `reply_to` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forum_index`
--

INSERT INTO `forum_index` (`id_forum`, `id_topics`, `deskripsi`, `create_date`, `create_by`, `action`, `reply_to`) VALUES
(1, 2, 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum optio explicabo fugit totam minima repellendus inventore rem dolore, minus mollitia vitae eaque, quaerat nisi sed, voluptatem consequuntur autem deserunt ratione!', '2020-08-29 00:00:00', 'jeff', 1, NULL),
(2, 2, 'ya okr', '2020-08-29 05:59:31', 'prakasbudiawan', 2, NULL),
(3, 2, 'asn', '2020-08-29 06:01:10', 'gunda;', 2, NULL),
(4, 2, 'polio', '2020-08-29 06:03:50', 'lo', 2, NULL),
(5, 2, 'mok', '2020-08-29 06:04:15', 'ki', 2, NULL),
(6, 2, 'gtyu', '2020-08-29 06:05:08', 'deftrer', 2, NULL),
(7, 2, 'derter', '2020-08-29 06:06:31', 'fret', 2, NULL),
(8, 4, 'tadi guru saya bolos', '2020-08-30 11:38:24', 'samual', 1, NULL),
(9, 2, 'ah udah biasa itu mah', '2020-08-30 11:47:07', 'panjul', 2, NULL),
(10, 5, 'batak ini why', '2020-09-17 10:51:28', 'juminten', 1, NULL),
(11, 6, 'hay', '2020-09-17 12:29:04', 'juminten', 1, NULL),
(12, 7, 'ansn', '2020-09-17 12:29:46', 'juminten', 1, NULL),
(13, 8, 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis cum vero reiciendis doloremque libero sequi, repudiandae dicta at dolorum unde, odio adipisci molestiae ad eveniet autem harum optio? Culpa, tempore?', '2020-09-17 12:54:43', 'juminten', 1, NULL),
(14, 2, 'lorem ? are ypu sure ?', '2020-09-18 07:43:57', 'dino', 2, NULL),
(15, 8, 'why ?', '2020-09-18 08:03:36', 'dino', 2, NULL),
(16, 8, 'damn boy >', '2020-09-18 08:53:37', 'makaveli', 2, NULL),
(17, 8, 'hellyeah', '2020-09-18 08:53:51', 'makaveli', 2, NULL),
(18, 8, 'are you joks ?', '2020-09-18 09:05:15', 'dino', 2, 'dino'),
(19, 8, 'xx', '2020-09-18 09:07:21', 'makaveli', 2, 'dino'),
(20, 8, 'yeah', '2020-09-18 09:07:47', 'dino', 2, 'dino'),
(21, 8, 'fuck', '2020-09-18 09:13:17', 'makaveli', 2, 'dino'),
(22, 8, 'hehe', '2020-09-18 09:13:30', 'dino', 2, 'dino'),
(23, 8, 'd', '2020-09-18 10:00:55', 'makaveli', 2, 'dino'),
(24, 8, 'zd', '2020-09-18 10:01:10', 'dino', 2, 'dino'),
(25, 8, 'kur', '2020-09-18 10:04:08', 'makaveli', 2, '15'),
(26, 9, 'SZfnjeFSJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJKJSNADJKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '2020-09-18 11:51:55', 'makaveli', 1, NULL),
(27, 9, 'its back', '2020-09-18 11:53:32', 'dino', 2, 'makaveli'),
(28, 9, 'oke', '2020-09-18 11:54:21', 'makaveli', 2, 'makaveli'),
(29, 9, 'haha', '2020-09-20 04:37:05', 'makaveli', 2, 'dino'),
(30, 8, 'haha', '2020-09-21 05:34:14', 'makaveli', 2, 'makaveli'),
(31, 8, 'adf', '2020-09-21 05:34:23', 'makaveli', 2, 'makaveli'),
(32, 8, 'dfad', '2020-09-21 05:34:27', 'makaveli', 2, 'makaveli'),
(33, 8, 'adf', '2020-09-21 05:34:33', 'makaveli', 2, 'makaveli'),
(34, 8, 'fsdf', '2020-09-21 05:34:42', 'makaveli', 2, 'makaveli'),
(35, 8, 'sdf', '2020-09-21 05:34:46', 'makaveli', 2, 'dino'),
(36, 8, 'sdf', '2020-09-21 05:34:52', 'makaveli', 2, 'makaveli'),
(37, 8, '42eae', '2020-09-21 05:34:59', 'makaveli', 2, 'makaveli'),
(38, 8, '234', '2020-09-21 05:35:05', 'makaveli', 2, 'makaveli'),
(39, 10, 'sansdmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfmfsl', '2020-09-21 09:08:50', 'makaveli', 1, NULL),
(40, 11, ' An MD5 hash is created by taking a string of an any length and encoding it into a 128-bit fingerprint. Encoding the same string using the MD5 algorithm will always result in the same 128-bit hash output. MD5 hashes are commonly used with smaller strings when storing passwords, credit card numbers or other sensitive data in databases such as the popular MySQL. This tool provides a quick and easy way to encode an MD5 hash from a simple string of up to 256 characters in length.\r\n\r\nMD5 hashes are also used to ensure the data integrity of files. Because the MD5 hash algorithm always produces the same output for the same given input, users can compare a hash of the source file with a newly created hash of the destination file to check that it is intact and unmodified.\r\n\r\nAn MD5 hash is NOT encryption. It is simply a fingerprint of the given input. However, it is a one-way transaction and as such it is almost impossible to reverse engineer an MD5 hash to retrieve the original string.', '2020-09-21 09:10:22', 'makaveli', 1, NULL),
(41, 2, 'haha', '2020-09-21 09:31:48', 'vandi', 2, 'dino');

-- --------------------------------------------------------

--
-- Table structure for table `forum_topics`
--

CREATE TABLE `forum_topics` (
  `id_topics` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` varchar(30) NOT NULL,
  `actions` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forum_topics`
--

INSERT INTO `forum_topics` (`id_topics`, `title`, `create_date`, `create_by`, `actions`) VALUES
(2, 'tews', '2020-08-29 00:00:00', 'jeff', '1'),
(3, 'BURHAN SAKIT', '2020-08-30 11:25:25', 'prakas', '1'),
(4, 'pa guru bolos', '2020-08-30 11:38:24', 'samual', '1'),
(5, 'pa batak why', '2020-09-17 10:51:28', 'juminten', '1'),
(6, 'tes blur', '2020-09-17 12:29:04', 'juminten', '1'),
(7, 'blur', '2020-09-17 12:29:46', 'juminten', '1'),
(8, 'test', '2020-09-17 12:54:43', 'juminten', '1'),
(9, '', '2020-09-18 11:51:55', 'makaveli', '1'),
(10, '', '2020-09-21 09:08:50', 'makaveli', '1'),
(11, '', '2020-09-21 09:10:22', 'makaveli', '1');

-- --------------------------------------------------------

--
-- Table structure for table `log_activity`
--

CREATE TABLE `log_activity` (
  `log_id` int(11) NOT NULL,
  `timestamp_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `haka` varchar(1000) NOT NULL,
  `create_by` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(30) NOT NULL,
  `haka` varchar(50) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`, `haka`, `description`) VALUES
(101, 'IT', 'HAKA 50', 'HANYA BISA AKSES MODUL TERTENTU'),
(205, 'SUPER ADMIN', 'FULL HAKA', 'SEMUA BISA DI AKSES KODE 205'),
(314, 'USER', 'NOT HAKA', 'USER BIASA BISANYA CUMA VIEW, SAMA DISKUSI DAN EDIT PROFILE SENDIRI');

-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE `user_activity` (
  `uid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pic` text NOT NULL,
  `is_login` varchar(3) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `is_blocked` varchar(3) NOT NULL,
  `role_id` int(15) NOT NULL,
  `password_fired` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_activity`
--

INSERT INTO `user_activity` (`uid`, `name`, `email`, `pic`, `is_login`, `username`, `password`, `create_date`, `update_date`, `is_blocked`, `role_id`, `password_fired`) VALUES
(1, 'makaveli', 'makaveli@superadmin.rocket', 'oke', 'N', 'makaveli', 'd3197fb64e031c6be6a9e2005a2fbe9a', '2020-09-01 00:00:00', '2020-09-15 08:50:02', 'N', 205, NULL),
(3, 'indon', 'indon@rocket', 'xx', 'N', 'indon', '6cfbec608383fd05c271de92010d455f', '2020-09-14 00:00:00', '2020-09-15 08:50:02', 'N', 341, NULL),
(4, 'vandi', 'detroit@rocket', 'si ajg.PNG', 'Y', 'vandi', 'e563ebd9e460b388e5979f7c7450c0b2', '2020-09-15 12:03:17', NULL, 'N', 101, 'Y'),
(5, 'fatur', 'sadnews', 'Cokelat_Daun_Salam_Agrikultur_Logo.png', 'N', 'fatur', 'bd32cbc1fb0f3af8ecfb58b8ad1929a8', '2020-09-15 12:55:27', NULL, 'N', 341, NULL),
(6, 'dino', 'dino@email.com', 'bbq5.jpg', 'N', 'dino', 'cecc4e5852b3b41209d94d6994e6822a', '2020-09-16 12:24:51', NULL, 'N', 341, NULL),
(7, 'juminten', 'juminten@gmail.com', 'bg_image1.jpg', 'Y', 'juminten', '5a8d72d30f075ea83afa9ac577d7d7dc', '2020-09-17 10:48:24', NULL, 'N', 341, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forum_index`
--
ALTER TABLE `forum_index`
  ADD PRIMARY KEY (`id_forum`);

--
-- Indexes for table `forum_topics`
--
ALTER TABLE `forum_topics`
  ADD PRIMARY KEY (`id_topics`);

--
-- Indexes for table `log_activity`
--
ALTER TABLE `log_activity`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forum_index`
--
ALTER TABLE `forum_index`
  MODIFY `id_forum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `forum_topics`
--
ALTER TABLE `forum_topics`
  MODIFY `id_topics` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `log_activity`
--
ALTER TABLE `log_activity`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=315;

--
-- AUTO_INCREMENT for table `user_activity`
--
ALTER TABLE `user_activity`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
