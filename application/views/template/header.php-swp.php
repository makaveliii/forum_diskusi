<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="images/jpg" href="<?php echo base_url(); ?>asset/images/aws.png">
    <title>APP-BRIGHT</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/plugin/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/datatables-bs4/css/dataTables.bootstrap4.css">
      <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/jquery.fancybox.css">

<body>
<div id="main">
    <nav class="navbar navbar-expand-lg navbar-light menu">
    <button class="openbtn" onclick="openNav()">☰</button> 
        <a class="navbar-brand logo" href="#"><img src="<?php echo base_url(); ?>asset/images/logo-nav.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url(); ?>dashboard">HOME</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url(); ?>sejarah/galeri">GALLERY</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    INFORMATION
                    </a>
                    <div class="dropdown-menu la" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url(); ?>sejarah">SEJARAH BATAK</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>sejarah/budaya">BUDAYA</a>
                        <!-- <a class="dropdown-item" href="<?php echo base_url(); ?>sejarah/wisata">DESTINASI WISATA</a> -->
                    </div>
                </li>
                <?php if ($this->session->userdata('status') != 'login') { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>auth">LOGIN</a>
                    </li>
                <?php }else{
                        if ($this->session->userdata('role') == '101') { ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                IT MANAGEMENT
                                </a>
                                <div class="dropdown-menu la" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#"> KELOLA DETINASI WISATA</a>
                                    <a class="dropdown-item" href="#"> KELOLA BUDAYA</a>
                                    <a class="dropdown-item" href="#"> KELOLA SEJARAH</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>forum">FORUM</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>auth/logout">LOGOUT</a>
                            </li>
                        <?php }else if ($this->session->userdata('role') == '341') { ?>
                            <!-- <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>auth">LOGIN</a>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>forum">FORUM</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>auth/logout">LOGOUT</a>
                            </li>
                        <?php }else{ ?>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ADMIN MANAGEMENT
                                </a>
                                <div class="dropdown-menu la" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>management"> KELOLA DATA IT</a>
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>management/log"> VIEW LOG ACTIVITY</a>
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>management/add"> ADD IT</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>forum">FORUM</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url(); ?>auth/logout">LOGOUT</a>
                            </li>
                        <?php }
                    }
                ?>
                
            </ul>
            <?php 
            if ($this->session->userdata('status') == 'login') { ?>
                <a href="<?php echo base_url(); ?>profil/index" class="yours"><i class="fa fa-users" aria-hidden="true">&nbsp;USER</i></a>
            <?php } ?>
        </div>
    </nav>
            </div>
    <div class="container mt-5"><div id="mySidebar" class="sidebar">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
        <a href="#">About</a>
        <a href="#">Services</a>
        <a href="#">Clients</a>
        <a href="#">Contact</a>
    </div>
    <!-- <button class="openbtn" onclick="openNav()">☰ Open Sidebar</button>  -->