
    <!-- <footer>
        <center>&copy; Copyright 2020 <a href="https://www.instagram.com/budiawanprakas_" class="mkv">Makaveli</a></center>
    </footer> -->
</div>

    <script src="<?php echo base_url(); ?>asset/js/jquery-3.4.1.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>asset/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script src="<?php echo base_url(); ?>asset/plugin/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/js/jquery.fancybox.js"></script>
    <script	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <script>
      $(document).ready(function(){
        openNav()
        var ctx = document.getElementById("myChart");
        var totalForum = $('#totalForum').val()
        var totalComment = $('#totalComment').val()
        var myChart = new Chart(ctx, {
          type: "doughnut",
          data: {
            labels: ["THREADS", "COMMENT"],
            datasets: [
              {
                label: "# of Tomatoes",
                data: [totalForum, totalComment],
                backgroundColor: [
                  "rgba(0, 206, 1, 0.2)",
                  "rgba(54, 162, 235, 0.2)",
                ],
                borderColor: [
                  "rgba(0, 206, 1, 1)",
                  "rgba(54, 162, 235, 1)",
                ],
                borderWidth: 1
              }
            ]
          },
          options: {
            //cutoutPercentage: 40,
            responsive: false,
            title: {
              display: true,
              position: "bottom",
              text: 'Forum Activity'
            },
          }
        });
        

        // total day

        // CSRF 
        var csfrData = {};
        csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
          url:"<?php echo  base_url(); ?>dashboard/totalDay",
          method:"POST",
          data:csfrData,
          dataType: 'json',
          success: function(msg){
            // console.log(data)
            var language = [];
            var total = [];
            var color = [];

            for(var count = 0; count < msg.length; count++)
            {
              language.push(msg[count].count_act);
            }
            // console.log(language)
            var dfv = document.getElementById("myBarChart");
            const x = {
              labels: ["January", "February", "Maret", "April", "May","June", "Jule", "Augustus", "September", "October", "November", "December"],
              datasets: [{
                label: '',
                data: language,
                backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(255, 159, 64, 0.2)',
                  'rgba(255, 205, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(201, 203, 207, 0.2)',
                  'rgba(255, 0, 0, 0.2)',
                  'rgba(246, 0, 255, 0.2)',
                  'rgba(30, 255, 0, 0.2)',
                  'rgba(255, 255, 255, 0.2)',
                  'rgba(0, 0, 0, 0.2)',
                ],
                borderColor: [
                  'rgb(255, 99, 132)',
                  'rgb(255, 159, 64)',
                  'rgb(255, 205, 86)',
                  'rgb(75, 192, 192)',
                  'rgb(54, 162, 235)',
                  'rgb(153, 102, 255)',
                  'rgb(201, 203, 207)',
                  'rgb(255, 0, 0)',
                  'rgb(246, 0, 255)',
                  'rgb(30, 255, 0)',
                  'rgb(255, 255, 255)',
                  'rgb(201, 203, 207)',
                ],
                borderWidth: 1
              }]
            };
            var myBarChart = new Chart(dfv, {
              type: 'bar',
              data: x,
              options: {
                scales: {
                  y: {
                    beginAtZero: true
                  },
                  xAxes: [{
                    scaleLabel: {
                      display: true,
                      labelString: 'Weekly Activity'
                    }
                  }],
                },
                legend : {
                  display : false

                }
              }
            });

          }
        })

      })
      function openNav() {
        document.getElementById("mySidebar").style.width = "200px";
        document.getElementById("main").style.marginLeft = "200px";
      }

      function closeNav() {
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
      }
      
    </script>
   
</body>
</html>