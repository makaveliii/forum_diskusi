<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="images/jpg" href="<?php echo base_url(); ?>asset/images/aws.png">
    <title>APP-BRIGHT</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/plugin/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/datatables-bs4/css/dataTables.bootstrap4.css">
      <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/jquery.fancybox.css">

<body>
<div id="main">
    <nav class="navbar navbar-expand-lg navbar-light menu">
        <button class="openbtn" onclick="openNav()">☰</button> 
        <a class="navbar-brand logo" href="#"><img id="sourcex" src="<?php echo base_url(); ?>asset/images/logo-nav.png" alt=""></a>

       
        <?php 
            if ($this->session->userdata('status') == 'login') { ?>
                <a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <!-- Dropdown link -->
                <img class="img img-responsive rounded-circle mb-3 ggff" src="<?php echo base_url(); ?>asset/upload/<?php echo $this->session->userdata('pic'); ?>" />
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?php echo base_url(); ?>profil">Profil</a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>auth/logout">Sign Out</a>
                </div>
        <?php } ?>
       
    </nav>
<!-- </div> -->
    <div class="container mt-5"><div id="mySidebar" class="sidebar">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
        <a href="<?php echo base_url(); ?>dashboard">Dashboard</a>
        <a href="<?php echo base_url(); ?>forum">Forum</a>
        <?php 
            if ($this->session->userdata('role') != '101') { ?>
                <a href="<?php echo base_url(); ?>sejarah/budaya">User Access</a>
        <?php } ?>
        <a href="<?php echo base_url(); ?>sejarah/budaya">Faq</a>
        <a href="<?php echo base_url(); ?>sejarah/budaya">Request</a>
    </div>
    <!-- <div class="contentx">
      
    </div> -->
    <!-- <button class="openbtn" onclick="openNav()">☰ Open Sidebar</button>  -->