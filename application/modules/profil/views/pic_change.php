<div class="men-in">
    <div class="card border-dark mb-3" style="max-width: 100%;">
        <div class="card-header bg-info"><i class="fas fa-edit" aria-hidden="true"> CHANGE PROFIL </i></div>
            <div class="card-body text-dark">
            <?php 
        if(isset($error))
        {
            echo "ERROR UPLOAD : <br/>";
            print_r($error);
            echo "<hr/>";
        }
        ?>
                <form action="<?php echo base_url(); ?>profil/change_pic" method="post" enctype="multipart/form-data">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="input-group mb-2">
                              <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-images" aria-hidden="true"></i></div>
                              </div>
                              <input type="hidden" name="uid" value="<?php echo $var; ?>">
                              <input type="file" name="pic" class="form-control" id="pic" placeholder="Your Pic">
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="add-tombol">
                        <input type="submit" value="Save" class="btn btn-danger" style="float:right;margin-left:5px;">
                    </div>
                </form>
                
            </div>
    </div>
</div>
