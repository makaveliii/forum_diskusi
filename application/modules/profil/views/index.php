<div class="men-in">
    <div class="row">
        <div class="col-md-4">

            <div class="card border-success mb-4">
                <div class="card-body text-center">

                    <img class="img img-responsive rounded-circle mb-3 dash-prof" src="<?php echo base_url(); ?>asset/upload/<?php echo $msg[0]['pic']; ?>" />
                    
                    <h3><?php echo  $msg[0]['username']; ?></h3>
                    <p><?php echo $msg[0]['email']; ?></p>
                    
                    <div class="tbl">
                        <p><a href="<?php echo base_url(); ?>auth/edit/<?php echo $msg[0]['uid']; ?>" class="btn btn-info" title="edit profil"><i class="fa fa-edit" aria-hidden="true"></i></a></p>
                        <p><a href="<?php echo base_url(); ?>profil/change/<?php echo $msg[0]['uid']; ?>" class="btn btn-warning" title="edit pic"><i class="fa fa-images" aria-hidden="true"></i></a></p>
                    </div>
        
                </div>
            </div>

            
        </div>


        <div class="col-md-8">

            <!-- <form action="" method="post" />
                <div class="form-group">
                    <textarea class="form-control" placeholder="Apa yang kamu pikirkan?"></textarea>
                </div>
            </form> -->

            <div class="card border-success mb-3" style="max-width: 100%;">
                <div class="card-header bg-dark" style="color:white;font-weight:bold;"> MY THREAD : <?php if ($msg_th > 0) { echo $msg_th; }else echo "<span style='color:red;'>YOU DON'T HAVE THREAD</span>";?> </div>
                
                <?php foreach($msg_thr as $data) { ?>

                <!-- <div class="card-body text-dark"> -->
                <div class="row ml-5 mt-4">
                    <div class="col-md-11">
                        <div class="card mb-3">
                            <div class="card-body" >
                                <?php if ($msg_th > 0) {
                                        if ($data['actions'] == 2) { ?>
                                            <button class="btn btn-info" style="float:right;">THIS THREAD WAS BLOCKED</button>
                                        <?php } else {
                                            echo $data['title']; ?>
                                            <button class="btn btn-success" style="float:right;">view</button>
                                        <?php }
                                    } ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php } ?>

            </div>

        </div>
    
    </div>
</div>