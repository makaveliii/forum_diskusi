<?php

class Profil_model extends CI_Model
{
    function get_profil($id)
    {
        $this->db->where('uid', $id);
        return $this->db->get('user_activity')->result_array();
    }

    function get_thr($id)
    {
        $this->db->select('*');
        $this->db->from('user_activity');
        $this->db->join('forum_topics', 'user_activity.username = forum_topics.create_by');
        $this->db->where('user_activity.username', $id);
        return $this->db->get()->result_array();
    }
    function get_thr2($id)
    {
        $this->db->select('*');
        $this->db->from('user_activity');
        $this->db->join('forum_topics', 'user_activity.username = forum_topics.create_by');
        $this->db->where('user_activity.username', $id);
        return $this->db->get()->num_rows();
    }
}