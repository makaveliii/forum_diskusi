<?php

class Profil extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Profil_model','prf');
        $this->load->library('form_validation');
        if ($this->session->userdata('status') != 'login') {
            redirect('auth');
        }
    }

    function index()
    {
        $id = $this->session->userdata('id');
        $user = $this->session->userdata('username');
        
        $data['msg'] = $this->prf->get_profil($id);
        $data['msg_thr'] = $this->prf->get_thr($user);
        $data['msg_th'] = $this->prf->get_thr2($user);
        $this->load->view('template/header');
        $this->load->view('index',$data);
        $this->load->view('template/footer');
        // print_r($data);
    }

    function change($id)
    {
        
        $uid['var'] = $id;
        $this->load->view('template/header');
        $this->load->view('pic_change',$uid);
        $this->load->view('template/footer');

    }

    function change_pic()
    {
        $config['upload_path']          = './asset/upload';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 1000;
		$config['max_width']            = 3000;
		$config['max_height']           = 2100;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('pic'))
		{
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('template/header');
				$this->load->view('pic_change', $error);
                $this->load->view('template/footer');
		}
		else
		{
            // $pass = $this->input->post('password');
            $date = date('Y-m-d h:i:s');
            // $email = $this->input->post('email');
            $uid = $this->input->post('uid');
			$image_data = $this->upload->data();
			$imgdata = file_get_contents($image_data['full_path']);
            $file_encode=base64_encode($imgdata);
            
            // $check_email = $this->Auth_model->cek_email($email);
            // $check_user = $this->Auth_model->cek_users($username);
            // if ( $check_user > 0) {
            //         echo "<script>alert('Username has been taken, please choose new');document.location='" . base_url('auth/register') . "'</script>";
            // }else{
			// $data['name'] = $this->input->post('name');
			// $data['email'] = $email;
			$data['pic'] = $this->upload->data('file_name');
			// $data['is_login'] = 'N';
			// $data['username'] =  $username;
			// $data['password'] =  md5($pass);
			// $data['create_date'] =  $date;
			// $data['is_blocked'] =  'N';
			// $data['role_id'] =  '341';
            // $this->db->insert('user_activity',$data);
                $this->db->where('uid', $uid);
                // print_r($data);
                $this->db->update('user_activity',$data);
			// unlink($image_data['full_path']);
            // redirect('dashboard');
                echo "<script>alert('Register is successfull');document.location='" . base_url('dashboard') . "'</script>";
            }
		
    }
}