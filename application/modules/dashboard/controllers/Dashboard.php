<?php


class Dashboard extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('status') != 'login') {
            redirect('auth');
        }
    }

    public function index()
    {
        $data = $this->getTotalCount();
        $this->load->view('template/header');
        $this->load->view('index', $data);
        $this->load->view('template/footer');
    }

    function getTotalCount()
    {
        // count forum
        $data = array();

        $this->db->select('count(*) x');
        $this->db->where('action', '1');
        $this->db->where('create_by', 'makaveli');
        $data['totalForum'] = $this->db->count_all_results('forum_index');

        // count comment
        $this->db->select('count(*) x');
        $this->db->where('action', '2');
        $this->db->where('create_by', 'makaveli');
        $data['totalComment'] = $this->db->count_all_results('forum_index');

        return $data;

    }

    function totalDay()
    {
        // count day
        $query = $this->db->query("select date_format(create_date,'%M') as count_month ,sum(action) as count_act
        from forum_index
        where create_by = 'makaveli'
        group by year(create_date),month(create_date)
        order by year(create_date),month(create_date)");

        $data = $query->result_array();

        // print_r($data); die();

        // return $data;
        echo json_encode($data);
    }

}