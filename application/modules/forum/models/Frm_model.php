<?php

class Frm_model extends CI_Model
{
    function getIndex()
    {
        return $this->db->get('forum_topics')->result_array();
    }

    function getTopics($id,$number,$offset)
    {
        
        // $this->db->select('*');
    //     return $this->db->query("SELECT forum_index.id_forum,forum_index.create_by,forum_index.id_topics,forum_index.deskripsi, forum_index.create_date,forum_index.action, forum_index.reply_to, forum_topics.title,user_activity.pic FROM
    //    forum_topics join forum_index ON forum_topics.id_topics = forum_index.id_topics join user_activity on user_activity.username = forum_index.create_by
    //     WHERE forum_index.id_topics = '$id'")->result_array();
        // return $this->db->query("SELECT forum_index.id_forum,forum_index.create_by,forum_index.id_topics,forum_index.deskripsi, forum_index.create_date,forum_index.action, forum_index.reply_to, forum_topics.title FROM
        // forum_index join forum_topics ON forum_index.id_topics = forum_topics.id_topics
        // WHERE forum_topics.id_topics = '$id'")->result_array();
        $this->db->select('forum_index.id_forum,forum_index.create_by,forum_index.id_topics,forum_index.deskripsi, forum_index.create_date,forum_index.action, forum_index.reply_to, forum_topics.title,user_activity.pic');
        $this->db->from('forum_topics');
        $this->db->join('forum_index', 'forum_index.id_topics = forum_topics.id_topics');
        $this->db->join('user_activity', 'user_activity.username = forum_index.create_by');
        $this->db->where('forum_index.id_topics',$id);
        $this->db->limit($number, $offset);
        return $this->db->get()->result_array();
    }

    function getpag($id)
    {
        
        // $this->db->select('*');
        return $this->db->query("SELECT forum_index.id_forum,forum_index.create_by,forum_index.id_topics,forum_index.deskripsi, forum_index.create_date,forum_index.action, forum_index.reply_to, forum_topics.title,user_activity.pic FROM
       forum_topics join forum_index ON forum_topics.id_topics = forum_index.id_topics join user_activity on user_activity.username = forum_index.create_by
        WHERE forum_index.action = 1")->num_rows();
    }

    function inputReply($table,$data)
    {
        return $this->db->insert($table, $data);
    }

    function insertTopics($table,$data)
    {
        $this->db->insert($table,$data);

        return $this->db->insert_id();
    }

    function insertForums($table,$data)
    {
        return $this->db->insert($table,$data);
    }

    function blocked_topics($id)
    {
        $this->db->set('actions', 2);
        $this->db->where('id_topics', $id);
        return $this->db->update('forum_topics');
    }
    function unblocked_topics($id)
    {
        $this->db->set('actions', 1);
        $this->db->where('id_topics', $id);
        return $this->db->update('forum_topics');
    }

    function get_threads($number,$offset)
    {
        $this->db->select('forum_index.id_forum,forum_index.create_by,forum_index.id_topics,forum_index.deskripsi, forum_index.create_date,forum_index.action, forum_index.reply_to, forum_topics.title,user_activity.pic');
        $this->db->from('forum_topics');
        $this->db->join('forum_index', 'forum_index.id_topics = forum_topics.id_topics');
        $this->db->join('user_activity', 'user_activity.username = forum_index.create_by');
        $this->db->where('forum_index.action', 1);
        $this->db->limit($number, $offset);
        return $this->db->get()->result_array();
    }
}