<script src="<?php echo base_url(); ?>asset/js/jquery-3.4.1.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>asset/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- <script src="<?php echo base_url(); ?>asset/plugin/js/bootstrap.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>asset/js/my.js"></script> -->
<script type="text/javascript">

$(document).ready(function() {
    var csfrData = {};
    csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';
    $.ajax({
        url : "<?php echo base_url(); ?>forum/viewss",
        method : 'post',
        dataType : 'json',
        data : csfrData,
        cache:false,
        success : function(data){

            $.each(data['msg'], function(index, value){
          
                let cut_str = value.deskripsi.slice(0, 90)
                let add_note = cut_str + '...'
                let date_format = new Date(value.create_date);
                let data_created =  date_format.getMonth()+'/'+ date_format.getDate()+'/'+date_format.getFullYear();
                $('.sins').append('<div class="card-body"> <div class="badan-forum" style="border-style: solid;border-color: #92a8d1;"><img src="<?php echo base_url(); ?>asset/upload/'+value.pic+'"> <div class="username">'+value.create_by+'</div><p class="deks" id="deskrip_forum" style="word-wrap:break-word;">'+add_note+'</p><hr><input type="hidden" name="id_topics" id="id_topics" value="'+value.id_topics+'"><span class="right">'+data_created+'</span><span class="left default" onclick=reply("'+value.id_topics+'")><i class="fa fa-comment-dots" aria-hidden="true" title="balas"></i></span></div></div>');
            });

            $('.pagination_links').append(data.links)
        
        }
    });
  })

function reply(trx)
{
    $('#exampleModal').modal('show');
    // alert(trx);
    $('#reply').val(trx);
}

function save()
{
    var id_topics = $('#id_topics').val();
    var user = $('#username').val();
    var deskripsi = $('#deskripsi').val();
    var reply_to = $('#reply').val();
    // alert(reply_to);

    $.ajax({
        url : "<?php echo base_url(); ?>forum/reply",
        method : 'post',
        dataType : 'json',
        data : {user:user,deskripsi:deskripsi,id_topics:id_topics,reply:reply_to},
        success : function(data){
                    if (data == true) {
                        alert('ok');
                    }else{
                        alert('data tak de');
                    }
                    $('#exampleModal').modal('hide');
                    window.location.reload();
        }
    });
  }

 
</script>