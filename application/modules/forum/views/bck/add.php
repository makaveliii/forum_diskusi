<div class="men-in">
   <div class="row feat">
        <div class="col-md-6">
            <form action="<?php echo base_url(); ?>forum/insert_title" method="post">
                <div class="card">
                    <h5 class="card-header">Add Topics To Forum</h5>
                    <div class="card-body">
                        <!-- <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">@</span>
                            </div> -->
                            <input type="hidden" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="username" value="<?php echo $this->session->userdata('username'); ?>">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
                        <!-- </div> -->
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user" aria-hidden="true"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="title" aria-label="title" aria-describedby="basic-addon1" name="title">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-circle" aria-hidden="true"></i></span>
                            </div>
                            <textarea class="form-control" aria-label="With textarea" id="deskripsi" name="deskripsi"></textarea>
                        </div>
                        <br>
                        <input type="submit" value="save" class="btn btn-warning">
                    </div>
                </div>
            </form>
        </div>
   </div>
</div>