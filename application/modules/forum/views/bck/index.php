
    <div class="men-in">

        <nav class="navbar navbar-expand-lg navbar-light menu-forum">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">  
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>forum/add_topics" style="color:white;font-size:14pt;background:teal;border-radius:3pt;">TOPICS</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link default" onclick="rep();" style="color:white;font-size:14pt;background:teal;border-radius:3pt;">REPORT</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link default" onclick="rules();" style="color:white;font-size:14pt;background:teal;border-radius:3pt;">RULES</a>
                    </li>
                </ul>
            </div>
        </nav>

        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Title Discuss</th>
                    <th>Owner</th>
                    <th>Created Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Title</th>
                    <th>Owner</th>
                    <th>Created Date</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>

    </div>

    <!-- Modal Rules -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Rules In Forum </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- <h3 class="title-rules"> Tata-tertib Forum </h3> -->
        <!-- <hr> -->
        <p> Setiap perkataan yang dilontarkan merupakan tanggung jawab setiap User, Admin akan melihat dan berhak melakukan penghapusan bahkan memblock user yang tidak mematuhi tata - tertib diforum ini. Dibawah ini adalah rules yang harus dipatuhi oleh user :</p>
        <ol>
            <li> Wajib Login dan Terdaftar Di Situs APP-BRIGHT</li>
            <li> Menggunakan Nama Dan Email yang valid</li>
            <li> Menggunakan foto Profil Real</li>
            <li> Dilarang menggunakan Kata - kata kasar saat di Forum</li>
            <li> Tidak diperbolehkan membuat thread yang sama, jika ada yang sama admin berhak menghapus salah satunya berdasarkan waktu yang lebih dahulu di buat</li>
        </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save_all" onclick="save();">Save changes</button>
      </div>
    </div>
  </div>
</div>

 <!-- Modal Rules -->
 <div class="modal fade" id="modal-report" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Report </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" id="username" value="<?php echo $this->session->userdata('username'); ?>">
        </div>
        <div class="form-group">
          <textarea class="form-control" aria-label="With textarea" id="report" name="report" placeholder="report input"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save_all" onclick="report();">Save changes</button>
      </div>
    </div>
  </div>
</div>