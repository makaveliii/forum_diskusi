<script src="<?php echo base_url(); ?>asset/js/jquery-3.4.1.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>asset/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- <script src="<?php echo base_url(); ?>asset/plugin/js/bootstrap.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>asset/js/my.js"></script> -->
<script type="text/javascript">
$(document).ready(function() {
    var t = $('#example').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url" : '<?php echo base_url(); ?>forum/load'
        },
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    });

    // t.on( 'order.dt search.dt', function () {
    //     t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
    //         cell.innerHTML = i+1;
    //     } );
    // } ).draw();
} );

function rules()
{
    $('#exampleModal').modal('show');
}

function close()
{
    $('#exampleModal').modal('hide');
}

function rep()
{
    $('#modal-report').modal('show');
}

function report()
{
    var rep = $('#report').val();
    var usr = $('#username').val();

    $.ajax({
        url : "<?php echo base_url(); ?>forum/report",
        method : 'post',
        dataType : 'json',
        data : {user:usr,rep:rep},
        success : function(data){
                    if (data == true) {
                        alert('Report is successfull');
                    }else{
                        alert('Failure Report, please check your data');
                    }
                    $('#report').modal('hide');
                    window.location.reload();
        }
    });
}
function close()
{
    $('#report').modal('hide');
}

function block(trx)
{
    var pork = trx;
    // alert(pork);
    var x = confirm('are you sure to block this user ? ');
    if (x == true) {
        $.ajax({
               url: '<?php echo base_url() ?>forum/blocked',
               data : {pork : pork},
               type: 'post',
               dataType : 'text',
               success: function(data) {
                    alert("User is Blocked");  
                    window.location.reload();
               },error:function(jqXHR, textStatus, errorThrown) {
                //tampilkan kode error
                alert('Error : '+jqXHR.status);
                }
            });
    }else{
        alert('blok has cancelled');
    }
}

function unblock(trx)
{
    var pork = trx;
    // alert(pork);
    var x = confirm('are you sure to unblock this user ? ');
    if (x == true) {
        $.ajax({
               url: '<?php echo base_url() ?>forum/unblocked',
               data : {pork : pork},
               type: 'post',
               dataType : 'text',
               success: function(data) {
                    alert("User is UnBlocked");  
                    window.location.reload();
               },error:function(jqXHR, textStatus, errorThrown) {
                //tampilkan kode error
                alert('Error : '+jqXHR.status);
                }
            });
    }else{
        alert('Unblok has cancelled');
    }
}

</script>