<?php

class Forum extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Frm_model', 'frm');
        if ($this->session->userdata('status') != 'login') {
            redirect('auth');
        }
    }

    public function index()
    {
        // $data['msg'] = $this->frm->getIndex();
        $this->load->view('template/header');
        $this->load->view('index');
        $this->load->view('dt_js');
        $this->load->view('template/footer');
    }

    function load()
    {

      
       
        $table =  'forum_topics';

        $primaryKey = '';
        
        $columns = array(
            array( 'db' => 'title', 'dt' => 0 ),
            array( 'db' => 'actions', 'dt' => ''),
            array( 'db' => 'create_by',  'dt' => 1 ),
        
            array( 'db' => 'create_date',     'dt' => 2 ),
            array(
                'db'        => 'id_topics',
                'dt'        => 3,
				'formatter' => function( $d, $row ) {
                    $ret ='';
                    $role = $this->session->userdata('role');
                    if ($row['actions'] =='1') {
                        $ret = '<a href="'.base_url().'forum/view/'.$d.'" class="btn btn-danger" style="margin-left:12px;padding:5px;"><i class="fa fa-eye" aria-hidden="true" title="View"></i></a>&nbsp;&nbsp;';
                    }else if(($row['actions'] =='2') && ($role == '341' || $role == '205')){
                        $ret = '<p class="btn btn-info">THIS THREAD WAS BLOCKED</p>';
                    }
                   if($row['actions'] =='1' && $role == '101'){
                    $ret .= '<button onclick="block('.$row['id_topics'].');" class="btn btn-dark" style="margin-left:12px;padding:5px;"><i class="fas fa-ban" title="block" aria-hidden="true"></i></button>';
                   }else if($row['actions'] =='2' && $role == '101'){
                    $ret .= '<button onclick="unblock('.$row['id_topics'].');" class="btn btn-info" style="margin-left:12px;padding:5px;"><i class="fas fa-lock" title="unblock" aria-hidden="true"></i></button>';
                   }

                    return $ret;
                })
        );

        $this->load->library('ssp');
        
        echo json_encode(
            $this->ssp->simple( $_GET, $this->db , $table, $primaryKey, $columns )
        );
    }

    function blocked()
    {
        $id = $this->input->post('pork');

        $this->frm->blocked_topics($id);
        echo $id;
    }

    function unblocked()
    {
        $id = $this->input->post('pork');

        $this->frm->unblocked_topics($id);
        echo $id;
    }

    function add_topics()
    {
        $this->load->view('template/header');
        $this->load->view('add');
        // $this->load->view('add_js');
        $this->load->view('template/footer');
    }

    function insert_title()
    {
        $username = $this->input->post('username');
        $title = $this->input->post('title');
        $deskripsi = $this->input->post('deskripsi');
        $date = date('Y-m-d h:i:s');
        // $ret = '';

        if ((!empty($username)) || (!empty($title)) || (!empty($deskripsi)) ) {
            if (strlen($deskripsi) > 120 ) {
                $data1 = array(
                    'title' => $title,
                    'create_date' => $date,
                    'create_by' => $username
                );
    
               $topics_insert = $this->frm->insertTopics('forum_topics',$data1);
    
                $data2 = array(
                    'id_topics' => $topics_insert,
                    'deskripsi' => $deskripsi,
                    'create_date' => $date,
                    'create_by' => $username,
                    'action' => 1
                );
    
                $this->frm->insertForums('forum_index',$data2);
    
                echo "<script>alert('Sucess added new discusion topics');document.location='" . base_url('forum') . "'</script>";
            }else{
                echo "<script>alert('Description Minimal 120 Characters');document.location='" . base_url('forum/add_topics') . "'</script>";
            }
        }else{
            echo "<script>alert('Added topics failed, please complite your data');document.location='" . base_url('forum/add_topics') . "'</script>";
        }
    }

    function view()
    {
        $uri = $this->uri->segment(3);


        $jumlah_data = $this->frm->getpag($uri);
		$this->load->library('pagination');
        $config['base_url'] = site_url('forum/view/'.$uri);
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 10;

        $config['full_tag_open'] = '<ul class="pagination">';        

        $config['full_tag_close'] = '</ul>';        

        $config['first_link'] = 'First';        

        $config['last_link'] = 'Last';        

        $config['first_tag_open'] = '<li>';        

        $config['first_tag_close'] = '</li>';        

        $config['prev_link'] = '&laquo';        

        $config['prev_tag_open'] = '<li class="prev">';        

        $config['prev_tag_close'] = '</li>';        

        $config['next_link'] = '&raquo';        

        $config['next_tag_open'] = '<li>';        

        $config['next_tag_close'] = '</li>';        

        $config['last_tag_open'] = '<li>';        

        $config['last_tag_close'] = '</li>';        

        $config['cur_tag_open'] = '<li class="active"><a href="#">';        

        $config['cur_tag_close'] = '</a></li>';        

        $config['num_tag_open'] = '<li>';        

        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);		
        $from = $this->uri->segment(4);
        $data['msg'] = $this->frm->get_threads($config['per_page'],$from);
        $data['links'] = $this->pagination->create_links(); 
        $this->load->view('template/header');
        $this->load->view('index',$data);
        $this->load->view('view_js');
        $this->load->view('template/footer');
   
    }

    function viewss()
    {
        $uri = $this->uri->segment(3);


        $jumlah_data = $this->frm->getpag($uri);
		$this->load->library('pagination');
        $config['base_url'] = site_url('forum/view/'.$uri);
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 5;

        $config['full_tag_open'] = '<ul class="pagination">';        

        $config['full_tag_close'] = '</ul>';        

        $config['first_link'] = 'First';        

        $config['last_link'] = 'Last';        

        $config['first_tag_open'] = '<li>';        

        $config['first_tag_close'] = '</li>';        

        $config['prev_link'] = '&laquo';        

        $config['prev_tag_open'] = '<li class="prev">';        

        $config['prev_tag_close'] = '</li>';        

        $config['next_link'] = '&raquo';        

        $config['next_tag_open'] = '<li>';        

        $config['next_tag_close'] = '</li>';        

        $config['last_tag_open'] = '<li>';        

        $config['last_tag_close'] = '</li>';        

        $config['cur_tag_open'] = '<li class="active"><a href="#">';        

        $config['cur_tag_close'] = '</a></li>';        

        $config['num_tag_open'] = '<li>';        

        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);		
        $from = $this->uri->segment(4);
        $data['msg'] = $this->frm->get_threads($config['per_page'],$from);
        $data['links'] = $this->pagination->create_links(); 
        // $this->load->view('template/header');
        // $this->load->view('index',$data);
        // $this->load->view('view_js');
        // $this->load->view('template/footer');

        echo json_encode($data);
   
    }

    function reply()
    {
        $user = $this->input->post('user');
        $deskripsi = $this->input->post('deskripsi');
        $id_topics = $this->input->post('id_topics');
        $reply = $this->input->post('reply');
        $date = date('Y-m-d h:i:s');

        $ret = '';
        if ( (!empty($user)) || (!empty($deskripsi)) ) {
            $data = array(
                'id_topics' => $id_topics,
                'deskripsi' => $deskripsi,
                'create_date' => $date,
                'create_by' => $user,
                'action' => 2,
                'reply_to'=>$reply
            );

            $this->frm->inputReply('forum_index',$data);
            $ret = true;
        }else{
            $ret = false;
        }
      
        echo json_encode($ret);
    }

    function tes()
    {
        $cc = $this->frm->get_threads(10,0);
        print_r($cc);
    }

    
}