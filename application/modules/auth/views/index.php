<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="images/jpg" href="<?php echo base_url(); ?>asset/images/aws.png">
    <title>APP-BRIGHT!</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/plugin/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
      <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/fontawesome-free/css/all.min.css">
</head>
<body class="login-box">
    


<!-- content wrapper -->

<div class="login">
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                LOGIN
            </div>
            <div class="card-body">
                <form action="" method="post">
                <?php form_open('no'); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
                                    <input type="text" name="username" class="form-control" id="username" placeholder="Username" autocomplete="off">
                                </div>
                                <?php echo form_error('username','<small class="text-danger pl-3">','</small>'); ?>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-unlock-alt" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" autocomplete="off">
                                </div>
                                <?php echo form_error('password','<small class="text-danger pl-3">','</small>'); ?>
                            </div>
                            <input type="submit" value="login" class="btn btn-success" id="nojob">
                            <p style="margin:auto;padding:3px;color:red;">Don't Have Account ? <b>Register</b> in <a href="<?php echo base_url(); ?>auth/register" class="btn btn-outline-dark btn-sm">Here</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end -->

    <script src="<?php echo base_url(); ?>asset/js/jquery-3.4.1.min.js"></script>
        <!-- DataTables -->
    <script src="<?php echo base_url(); ?>asset/plugin/js/bootstrap.min.js"></script>
</body>
</html>