<div class="men-in">
    <div class="card border-dark mb-3" style="max-width: 100%;">
        <div class="card-header bg-info"><i class="fas fa-edit" aria-hidden="true"> REGISTER HERE </i></div>
            <div class="card-body text-dark">
            <?php 
        if(isset($error))
        {
            echo "ERROR UPLOAD : <br/>";
            print_r($error);
            echo "<hr/>";
        }
        ?>
                <form action="<?php echo base_url(); ?>auth/proses_reg" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name">
                                </div>
                                <!-- <?php echo form_error('name','<small class="text-danger pl-3">','</small>'); ?> -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-signature" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="text" name="username" class="form-control" id="username" placeholder="Your Username">
                                </div>
                                <!-- <?php echo form_error('username','<small class="text-danger pl-3">','</small>'); ?> -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group"> 
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-key" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Your Password">
                                </div>
                                <!-- <?php echo form_error('password','<small class="text-danger pl-3">','</small>'); ?> -->
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-images" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="file" name="pic" class="form-control" id="pic" placeholder="Your Pic">
                                    <!-- <input type="file" name="berkas" /> -->
                                </div>
                                <!-- <?php echo form_error('pic','<small class="text-danger pl-3">','</small>'); ?> -->
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-envelope" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="text" name="email" class="form-control" id="email" placeholder="Your Email">
                                </div>
                                <!-- <?php echo form_error('Email','<small class="text-danger pl-3">','</small>'); ?> -->
                            </div>
                        </div>
                    </div>
                    <div class="add-tombol">
                        <input type="submit" value="Save" class="btn btn-danger" style="float:right;margin-left:5px;">
                    </div>
                </form>
            </div>
    </div>
</div>
