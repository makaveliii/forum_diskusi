<div class="men-in">
    <div class="card border-dark mb-3" style="max-width: 100%;">
        <div class="card-header bg-warning"><i class="fas fa-edit" aria-hidden="true"> EDIT PROFIL </i></div>
            <div class="card-body text-dark">
          
                <form action="<?php echo base_url(); ?>auth/edit_proses" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-user" aria-hidden="true" title="your name"></i></div>
                                    </div>
                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
                                    <input type="hidden" name="id" value="<?php echo $msg[0]['uid']; ?>">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" value="<?php echo $msg[0]['name']; ?>">
                                </div>
                                <!-- <?php echo form_error('name','<small class="text-danger pl-3">','</small>'); ?> -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-signature" aria-hidden="true" title="your Username"></i></div>
                                    </div>
                                    <input type="text" name="username" class="form-control" id="username" placeholder="Your Username" value="<?php echo $msg[0]['username']; ?>">
                                </div>
                                <!-- <?php echo form_error('username','<small class="text-danger pl-3">','</small>'); ?> -->
                            </div>
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group"> 
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-key" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="New Password">
                                </div>
                                <small class="text-primary pl-3">*) jika tidak ingin merubah  password, kosongkan saja field ini</small>
                                <!-- <?php echo form_error('password','<small class="text-danger pl-3">','</small>'); ?> -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-envelope" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="text" name="email" class="form-control" id="email" placeholder="Your Email" value="<?php echo $msg[0]['email']; ?>">
                                </div>
                                <!-- <?php echo form_error('Email','<small class="text-danger pl-3">','</small>'); ?> -->
                            </div>
                        </div>
                    </div>
                    <div class="add-tombol">
                        <input type="submit" value="Save" class="btn btn-danger" style="float:right;margin-left:5px;">
                    </div>
                </form>
            </div>
    </div>
</div>
