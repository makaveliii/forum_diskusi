<?php

class Auth_model extends CI_Model
{
    function cek_user($table,$d)
    {
        return $this->db->get_where($table,$d);
    }

    function cek_akses($username,$password)
    {
       
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get('user_activity');

        return $query->result_array();
    
    }
    function cek_akses2($id)
    {
        $this->db->select('*');
        $this->db->where('uid', $id);
        $query = $this->db->get('user_activity');

        return $query->result_array();
    }

    function ch_update_pass($id)
    {
        $data = array(
            'is_login' => 'Y'
        );
        $this->db->where('uid', $id);
        return $this->db->update('user_activity', $data);
    }

    function get_uid($id)
    {
        $this->db->select('*');
        $this->db->where('uid', $id);
        $query = $this->db->get('user_activity');

        return $query->result_array();
    }

    function cek_psw($table,$data)
    {
        return $this->db->get($table,$data);
    }

    function cek_upd($id,$new_pas)
    {
        $data = array(
            'password' => $new_pas,
            'password_fired' => 'Y',
            'is_login' => 'Y'
        );

        $this->db->where('uid', $id);
        return $this->db->update('user_activity', $data);
    }

    function update_login($id)
    {
        $data = array(
            'is_login' => 'N'
        );

        $this->db->where('uid', $id);
        return $this->db->update('user_activity', $data);
    }

    function cek_email($id)
    {
        $this->db->where('email', $id);
        return $this->db->get('user_activity')->num_rows();
    }

    function cek_users($id)
    {
        $this->db->where('username', $id);
        return $this->db->get('user_activity')->num_rows();
    }

    function cek_edit($id)
    {
        $this->db->where('uid', $id);
        return $this->db->get('user_activity')->result_array();
    }


}