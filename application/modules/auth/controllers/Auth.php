<?php

class Auth extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Auth_model');
    }

    function index()
    {
        if ($this->session->userdata('status') == 'login') {
            redirect('dashboard');
        }
        $this->form_validation->set_rules('username', 'username ','required');
        $this->form_validation->set_rules('password', 'password','required');
        if ($this->form_validation->run()==false) {
            $title = array(
                'tittle' => 'no'
            );
            $this->load->view('index',$title);

        }else{
            $this->auth_proc();
        }
    }

    function auth_proc()
    {
        error_reporting(0);
        $username = htmlspecialchars(filter_var($this->input->post('username'), FILTER_SANITIZE_STRING));
        $pw_salt = 'Urururu90182';
        $password = htmlspecialchars(md5(filter_var($pw_salt.$this->input->post('password'), FILTER_SANITIZE_STRING)));
        $token = htmlspecialchars($this->input->post('csrf_token'));

        $d = array(
            'username' => $username,
            'password' => $password
        );

        $cek_pass = $this->Auth_model->cek_user('user_activity',$d)->num_rows();
        $cek_akses = $this->Auth_model->cek_akses($username,$password);
        $akses = $cek_akses[0]['role_id'];
        $user = $cek_akses[0]['username'];
        $nama = $cek_akses[0]['nama'];
        $id = $cek_akses[0]['uid'];
        $fired = $cek_akses[0]['password_fired'];
        $pic = $cek_akses[0]['pic'];
        $is_block = $cek_akses[0]['is_blocked'];
        $email = $cek_akses[0]['email'];

        $sess_data = array(
            'username' => $user,
            'id'=>$id,
            'email'=>$email,
            'nama' => $nama,
            'role'=>$akses,
            'pic'=>$pic,
            'status' => 'login'
        );

        if ($cek_pass > 0) {
          
            if ($is_block != 'Y') {

                    if (($akses == '341') || ($akses == '205') ) {
    
                        $this->session->set_userdata($sess_data);
                        $this->Auth_model->ch_update_pass($id);
                        echo "<script>alert('Login is Successfull!!');document.location='" . base_url('dashboard') . "'</script>";
                       
                    }else if($akses == '101'){
        
                        if ($fired == 'Y' ) {
        
                            $this->session->set_userdata($sess_data);
                            $this->Auth_model->ch_update_pass($id);
                            echo "<script>alert('Youre Login!!');document.location='" . base_url('dashboard') . "'</script>";
        
                        }else{
                            redirect('auth/reset_pass/'.$id);
                        }
        
                        
                    }
                
            }else{
                echo "<script>alert('Your Account is Blocked, Please Contact  Administrator');document.location='" . base_url('auth') . "'</script>";
            }

        }else{

            echo "<script>alert('You are not registered!!');document.location='" . base_url('auth') . "'</script>";
        }
    }

    function reset_pass()
    {
        error_reporting(0);
        $id_s = $this->uri->segment('3');

        
        $this->form_validation->set_rules('username', 'username ','required');
        $this->form_validation->set_rules('new_password', 'new_password','required');
       
        if ($this->form_validation->run()==false) {
            $title = array(
                'tittle' => 'change'
            );
            $get_ui['lumia'] = $this->Auth_model->get_uid($id_s);
        $this->load->view('pass_update',$get_ui);


        }else{
            $this->validasi_res();
        }

    }

    function validasi_res()
    {
        error_reporting(0);
        $id = $this->input->post('id');
        $username = $this->input->post('username');
        $new_password = $this->input->post('new_password');
        $token = htmlspecialchars($this->input->post('csrf_token'));
        $pw_salt = 'Urururu90182';
        $password = md5($pw_salt.$new_password);
        

        $cek_arr = array(
            'user_id' => $id,
            'username' => $username
        );
        $cek_us = $this->Auth_model->cek_psw('user_activity',$cek_arr)->num_rows();
        if($cek_us > 0)
        {
            $cek_upd = $this->Auth_model->cek_upd($id,$password);

            if ($cek_upd) {

                    $cek_akses = $this->Auth_model->cek_akses2($id);
                    $akses = $cek_akses[0]['role_id'];
                    $user = $cek_akses[0]['username'];
                    $nama = $cek_akses[0]['nama'];
                    $id = $cek_akses[0]['uid'];
                    $fired = $cek_akses[0]['password_fired'];
                    $is_log = $cek_akses[0]['is_login'];
                    $email = $cek_akses[0]['email'];

                    $sess_data = array(
                        'username' => $user,
                        'id'=>$id,
                        'nama' => $nama,
                        'email'=>$email,
                        'role'=>$akses,
                        'status' => 'login'
                    );

                    $this->session->set_userdata($sess_data);
                    echo "<script>alert('Berhasil Login');document.location='" . base_url('dashboard') . "'</script>";
            }

        }
    }

    function register()
    {
        $this->load->view('template/header');
        $this->load->view('register');
        $this->load->view('template/footer');
    }

    function logout()
    {
        $id = $this->session->userdata('id');
        $this->session->sess_destroy();
        $this->Auth_model->update_login($id);
        redirect('auth');
    }

    function proses_reg()
    {
        $config['upload_path']          = './asset/upload';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 1000;
		$config['max_width']            = 1080;
		$config['max_height']           = 1090;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('pic'))
		{
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('template/header');
				$this->load->view('register', $error);
                $this->load->view('template/footer');
		}
		else
		{
            $pass = htmlspecialchars(filter_var($this->input->post('password'),FILTER_SANITIZE_STRING));
            $date = date('Y-m-d h:i:s');
            $email = htmlspecialchars(filter_var($this->input->post('email'), FILTER_SANITIZE_STRING));
            $username = htmlspecialchars(filter_var($this->input->post('username'), FILTER_SANITIZE_STRING));
            $token = $this->input->post('csrf_token');
			$image_data = $this->upload->data();
			$imgdata = file_get_contents($image_data['full_path']);
            $file_encode=base64_encode($imgdata);
            
            $check_user = $this->Auth_model->cek_users($username);
            if ( $check_user > 0) {
                    echo "<script>alert('Username has been taken, please choose new');document.location='" . base_url('auth/register') . "'</script>";
            }else{
            $pw_salt = 'Urururu90182';
			$data['name'] = $this->input->post('name');
			$data['email'] = $email;
			$data['pic'] = $this->upload->data('file_name');
			$data['is_login'] = 'N';
			$data['username'] =  $username;
			$data['password'] =  md5($pw_salt.$pass);
			$data['create_date'] =  $date;
			$data['is_blocked'] =  'N';
			$data['role_id'] =  '101';
			$this->db->insert('user_activity',$data);

                echo "<script>alert('Register is successfull');document.location='" . base_url('dashboard') . "'</script>";
            }
		}
    }

    function edit()
    {
        $id = $this->uri->segment(3);
        $data['msg'] = $this->Auth_model->cek_edit($id);
        $this->load->view('template/header');
        $this->load->view('edit_profil', $data);
        $this->load->view('template/footer');
    }

    function edit_proses()
    {
        $pass = $this->input->post('password');
        $date = date('Y-m-d h:i:s');
        $email = $this->input->post('email');
        $uid = $this->input->post('id');
        $username = $this->input->post('username');
        $email_check = $this->session->userdata('email');
        $user_check = $this->session->userdata('username');
           
            if (empty($pass))
            {
                $data['name'] = $this->input->post('name');
                $data['email'] = $email;
                $data['username'] =  $username;
                $data['update_date'] =  $date;
                $data['uid'] = $uid;
            }else{
                $pw_salt = 'Urururu90182';
                $data['name'] = $this->input->post('name');
                $data['email'] = $email;
                $data['username'] =  $username;
                $data['password'] =  md5($pw_salt.$pass);
                $data['update_date'] =  $date;
            }
            if ( $username == $user_check ) {
               
        
                $this->db->where('uid', $uid);
                $this->db->update('user_activity',$data);
            }else{
                $check_user = $this->Auth_model->cek_users($username);
                if ($check_user > 0) {
                        echo "<script>alert('Username has been taken, please choose new');document.location='" . base_url('profil') . "'</script>";
                }else{
                    $this->db->where('uid', $uid);
                    $this->db->update('user_activity',$data);
                }
            }
            echo "<script>alert('Update is successfull');document.location='" . base_url('profil') . "'</script>";
        
    }

}