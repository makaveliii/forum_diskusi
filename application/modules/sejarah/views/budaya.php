
    <div class="men-in">
        <div class="card border-dark mb-3" style="max-width: 100%;">
            <div class="card-header bg-success">THE BUDAYA OF BATAK TRIBE</div>
                <div class="card-body text-dark">
                    <div class="sjr">
                        <img src="<?php echo base_url(); ?>asset/images/budaya.jpg" alt="" class="srj-on">
                        <h2 id="por-tit">1. Dilarang menikahi satu marga</h2>
                        <hr style="width:60%;text-align:right;margin-left:0">
                        <p class='od'>Menikah dengan lelaki atau peremouan satu marga sangat terlarang dikebudayaan orang batak urbanreaders.Walaupun berasal dari keluarga yang tidak sama bahkan tidak ada hubungan keluarga sama sekali, menikah dengan orang yang semarga tidak diperbolehkan. Menurut adat, orang yang memiliki marga yang sama walaupun tidak dalam keluarga yang sama tetaplah memiliki hubungan saudara dan dianggap merusak silsilah. Nah, bukan hanya semarga aja nih guys, namun ada juga beberapa marga yang berbeda tapi masih dalam hubungan silsilah yang sama juga tidak diperbolehkan untuk menikah. Wah, jadi dari awal pendekatan harus ditanya ya marganya apa agar tidak terjadi hal-hal yang tidak dinginkan seperti tidak mendapatkan restu. Pada abad ke-6, pedagang-pedagang Tamil asal India mendirikan kota dagang bernama Barus, yang terletak di pesisir barat Sumatra Utara. Mereka berdagang kapur Barus yang diusahakan oleh petani-petani di pedalaman. Kapur Barus dari tanah Batak bermutu tinggi sehingga menjadi salah satu komoditas ekspor di samping kemenyan. Pada abad ke-10, Barus diserang oleh Sriwijaya.</p>
                        <h2 id="por-tit">2. Menikahi Pariban/Sepupu</h2>
                        <hr style="width:100%;text-align:right;margin-left:0">
                        <p class='od'>Alih-alih mencari jodoh dari keluarga lain, pariban dianggap sebagai jodoh yang pantas karena sudah diketahui nih asal usul keluarganya. Pariban atau sepupu yang boleh dinikahi juga bukan sembarang sepupu loh guys. Untuk anak laki-laki pariban yang boleh dinikahi adalah anak dari saudara laki-laki ibu sedangkan anak perempuan boleh menikah dengan anak dari saudara perempuan ayah.</p>
                        <h2 id="por-tit">3. Martarombo</h2>
                        <hr style="width:100%;text-align:right;margin-left:0">
                        <p class='od'>Martarombo artinya berbincang-bincang untuk mencari hubungan silsilah keluarga. Biasanya jika baru bertemu dengan orang baru sesama suku batak, mereka suka menanyakan marga kita, marga ibu sampai marga pasangan kita. Hal ini bertujuan agar bisa menemukan hubungan pertalian persaudaraan berdasarkan marga. Selain itu, martarombo juga merupkan adat istiadat yang tetap digunakan samoai sekarang karena dengan mengetahui silsilah marga kita bisa memanggil seseorang dengan sebutan yang tepat seperti pariban, iboto, tulang, bou, amangboru, dan lain sebagainya.</p>
                        <h2 id="por-tit">4. Mangulosi</h2>
                        <hr style="width:100%;text-align:right;margin-left:0">
                        <p class='od'>Nah, dalam upacara atau acara-acara yang dilaksanakan orang batak pasti kalian sudah tidak asing kan urbanreaders dengan ulos yang biasanya digunakan.Ulos yang merupkan kain tradisional batak ini digunakan untuk beberapa hal sesuai dengan fungsinya, seperti upacara pernikahan, upacara adat, maupun kematian. Nah ulos yang digunakan juga berbeda-beda jenisnya loh, selain perbedaan untuk penggunaanya, perbedaan jenis ulos juga menunjukkan strata seseorang dalam lingkungan sosial.</p>
                        <h2 id="por-tit">5. Tuhor</h2>
                        <hr style="width:100%;text-align:right;margin-left:0">
                        <p class='od'>Tuhor atau mahar memang selalu seru untuk diperbincangkan, apalagi dikalangan anak-anak muda batak.Keluarga yang masih sangat memegang erat kebudayaan batak ini biasanya akan menentukan tuhor yang semakin mahal sesuai dengan tingkat pendidikan anaknya.Tuhor ini nantinya akan digunakan untuk keperluan pesta, perlengkapan pernikahan dan lain-lain.Tapi dijaman yang sudah millenial ini, sudah banyak keluarga dari suku batak yang meninggalkan kebiasaan ini karena dianggap hanya akan mempersulit pernikahan antara dua orang yang sudah saling mencintai.</p>
                        <hr style="width:100%;text-align:right;margin-left:0">
                        <strong>sumber <a href="https://www.urbanasia.com/mengenal-kebudayaan-suku-batak-U2833" class="identity"><i>Urbanasia</i></a></strong>
                    </div>
                </div>
        </div>
    </div>
