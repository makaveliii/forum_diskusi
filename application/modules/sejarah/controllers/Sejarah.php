<?php

class Sejarah extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('status') != 'login') {
            redirect('auth');
        }
    }

    function index()
    {
        $this->load->view('template/header');
        $this->load->view('index');
        $this->load->view('template/footer');
    }

    function budaya()
    {
        $this->load->view('template/header');
        $this->load->view('budaya');
        $this->load->view('template/footer');
    }

    function galeri()
    {
        $this->load->view('template/header');
        $this->load->view('galeri');
        $this->load->view('dt_js');
        $this->load->view('template/footer'); 
    }

}