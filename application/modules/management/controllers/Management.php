<?php

class Management extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Management_model', 'mm');
        if ($this->session->userdata('status') != 'login') {
            redirect('auth');
        }
    }

    function index()
    {
        $data['msg'] = $this->mm->get_it()->result_array();
        $this->load->view('template/header');
        $this->load->view('index',$data);
        $this->load->view('dt_js');
        $this->load->view('template/footer');
    }

    function edit()
    {
        $uri = $this->uri->segment('3');

        $data['msg'] = $this->mm->get_uid($uri);
        $this->load->view('template/header');
        $this->load->view('edit',$data);
        $this->load->view('template/footer');

    }

    function update()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');

        $this->mm->update_sa($id);

        echo "<script>alert('berhasil update');document.location='" . base_url('management') . "'</script>";
    }

    function blocked()
    {
        $var = $this->input->post('pork');
        $this->mm->update_block($var);
        echo $var;
    }

    function unblocked()
    {
        $var = $this->input->post('pork');
        $this->mm->update_unblock($var);
        echo $var;
    }

    function delete()
    {
        $var = $this->input->post('pork');
        $this->mm->delete_it($var);
        echo $var;
    }

    function add()
    {
        $this->load->view('template/header');
        $this->load->view('add');
        $this->load->view('template/footer');
    }

    function proses_it()
    {
        $config['upload_path']          = './asset/upload';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('pic'))
		{
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('template/header');
				$this->load->view('add', $error);
                $this->load->view('template/footer');
		}
		else
		{
            $pass = $this->input->post('password');
            $date = date('Y-m-d h:i:s');
			$image_data = $this->upload->data();
			$imgdata = file_get_contents($image_data['full_path']);
			$file_encode=base64_encode($imgdata);
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['pic'] = $this->upload->data('file_name');
			$data['is_login'] = 'N';
			$data['username'] =  $this->input->post('username');
			$data['password'] =  md5($pass);
			$data['create_date'] =  $date;
			$data['is_blocked'] =  'N';
			$data['role_id'] =  '101';
			$this->db->insert('user_activity',$data);
			unlink($image_data['full_path']);
			redirect('dashboard');
		}
    }
    

}