<script src="<?php echo base_url(); ?>asset/js/jquery-3.4.1.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url(); ?>asset/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>asset/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- <script src="<?php echo base_url(); ?>asset/plugin/js/bootstrap.min.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>asset/js/my.js"></script> -->
<script type="text/javascript">
$(document).ready(function() {
    var t = $('#example').DataTable();
} );

function block(trx)
{
    var pork = trx;
    var x = confirm('are you sure to block this user ? ');
    if (x == true) {
        $.ajax({
               url: '<?php echo base_url() ?>management/blocked',
               data : {pork : pork},
               type: 'post',
               dataType : 'text',
               success: function(data) {
                    alert("User is Blocked");  
                    window.location.reload();
               },error:function(jqXHR, textStatus, errorThrown) {
                //tampilkan kode error
                alert('Error : '+jqXHR.status);
                }
            });
    }else{
        alert('blok has cancelled');
    }
}

function unblock(trx)
{
    var pork = trx;
    var x = confirm('are you sure to unblock this user ? ');
    if (x == true) {
        $.ajax({
               url: '<?php echo base_url() ?>management/unblocked',
               data : {pork : pork},
               type: 'post',
               dataType : 'text',
               success: function(data) {
                    alert("User is Unblocked");  
                    window.location.reload();
               },error:function(jqXHR, textStatus, errorThrown) {
                //tampilkan kode error
                alert('Error : '+jqXHR.status);
                }
            });
    }else{
        alert('unblok has cancelled');
    }
}

function deletex(trx)
{
    var pork = trx;
    var x = confirm('are you sure to Delete this user ? ');
    if (x == true) {
        $.ajax({
               url: '<?php echo base_url() ?>management/delete',
               data : {pork : pork},
               type: 'post',
               dataType : 'text',
               success: function(data) {
                    alert("User is Delete");  
                    window.location.reload();
               },error:function(jqXHR, textStatus, errorThrown) {
                //tampilkan kode error
                alert('Error : '+jqXHR.status);
                }
            });
    }else{
        alert('Delete has cancelled');
    }
}




</script>