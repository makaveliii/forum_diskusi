<div class="men-in">
    <div class="card border-dark mb-3" style="max-width: 100%;">
        <div class="card-header bg-danger">MANAGEMENT EDIT IT</div>
            <div class="card-body text-dark">
                <form action="<?php echo base_url(); ?>management/update" method="post">
                   <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <?php foreach($msg as $data) : ?>
                                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
                                <input type="text" name="name" class="form-control" value="<?php echo $data['name']; ?>">
                                <input type="hidden" name="id" class="form-control" value="<?php echo $data['uid']; ?>">
                                <?php endforeach; ?>
                            </div>
                        </div>
                            <div class="col-md-3">
                                <input type="submit" class="btn btn-primary" value="proses to super admin">
                            </div>
                    </div>
                </form>
            </div>
    </div>
</div>