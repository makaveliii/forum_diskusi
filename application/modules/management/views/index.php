<div class="men-in">
    <div class="card border-dark mb-3" style="max-width: 100%;">
        <div class="card-header bg-danger">ADMIN MANAGEMENT IT</div>
            <div class="card-body text-dark">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>It Name</th>
                            <th>Email</th>
                            <th>Blocked</th>
                            <th>Is Login</th>
                            <th>Update</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($msg as $data) { ?>
                        <tr>
                            <td> <?php echo $no++; ?> </td>
                            <td> <?php echo $data['name']; ?> </td>
                            <td> <?php echo $data['email']; ?> </td>
                            <td> <?php echo $data['is_blocked']; ?> </td>
                            <td> <?php echo $data['is_login']; ?> </td>
                            <td> <?php echo $data['update_date']; ?> </td>
                        <td> 
                            <a href="<?php echo base_url(); ?>management/edit/<?php echo $data['uid']; ?>" class="btn btn-warning" style="margin-left:12px;padding:5px;"><i class="fas fa-edit" title="edit" aria-hidden="true"></i></a> 
                            <button onclick="deletex(<?php echo $data['uid']; ?>);" class="btn btn-danger" style="margin-left:12px;padding:5px;"><i class="fas fa-trash" title="delete" aria-hidden="true"></i>
                            <?php if ($data['is_blocked'] == 'Y') { ?>
                                <button onclick="unblock(<?php echo $data['uid']; ?>);" class="btn btn-info" style="margin-left:12px;padding:5px;"><i class="fas fa-unlock" title="unblocked" aria-hidden="true"></i></button></td>
                            <?php }else{ ?>
                                <button onclick="block(<?php echo $data['uid']; ?>);" class="btn btn-dark" style="margin-left:12px;padding:5px;"><i class="fas fa-ban" title="block" aria-hidden="true"></i></button></td>
                            <?php } ?>
                            
                        </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>It Name</th>
                            <th>Email</th>
                            <th>Blocked</th>
                            <th>Is Login</th>
                            <th>Update</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
        </div>
    </div>
</div>
