<?php

class Management_model extends CI_Model
{
  function get_it()
  {
    //   $this->db->form('*');
      $this->db->where('role_id', '101');
      return $this->db->get('user_activity');
  }

  function get_uid($id)
  {
      $this->db->where('uid', $id);
      return $this->db->get('user_activity')->result_array();
  }

  function update_sa($id)
  {
      $date = date('Y-m-d h:i:s');
    $this->db->set('role_id', '205');
    $this->db->set('update_date', $date);
    $this->db->where('uid', $id);
    return $this->db->update('user_activity');
  }

  function update_block($id)
  {
      $date = date('Y-m-d h:i:s');
    $this->db->set('is_blocked', 'Y');
    $this->db->where('uid', $id);
    return $this->db->update('user_activity');
  }

  function update_unblock($id)
  {
      $date = date('Y-m-d h:i:s');
    $this->db->set('is_blocked', 'N');
    $this->db->where('uid', $id);
    return $this->db->update('user_activity');
  }

  function delete_it($id)
  {
      $this->db->where('uid', $id);
      return $this->db->delete('user_activity');
  }

}